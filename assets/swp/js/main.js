$(function () {
    // calling outdated browser plugin 
    outdatedBrowser({
        bgColor: '#f25648',
        color: '#ffffff',
        lowerThan: 'transform',
        languagePath: swp.base_url + 'assets/swp/outdated_browser/lang/en.html'
    });
    // setting file upload browser button style      
    $(":file").filestyle({input: false, iconName: "fa fa-upload", buttonText: "Browse", buttonName: "btn-primary"});
    // setting CK Editor
    if ($('#articleEditor').length) {
        var editor = CKEDITOR.replace('articleEditor', {
            language: 'es',
            uiColor: '#FFFFFF',
            height: 300,
            toolbarCanCollapse: true,
            extraPlugins: '',
            uploadUrl: '/uploader/upload.php',
            imageUploadUrl: '/uploader/upload.php?type=Images',
            filebrowserImageUploadUrl: '/imgbrowse/imgbrowse.php',
            filebrowserImageBrowseUrl: '/imgbrowse/imgbrowse.html?imgroot=http://localhost:800/simplewebpanel/assets/swp/img/',
            embed_provider: '//ckeditor.iframe.ly/api/oembed?url={url}&callback={callback}',
            extraAllowedContent: 'iframe style;*[id,rel](*)',
            // Load the default contents.css file plus customizations for this sample.
            contentsCss: [CKEDITOR.basePath + 'contents.css']
        });
    }
    // selectize plugin style for all dropdown    
    $(".swp-dropdown").selectize();
    // setting functionality for popup confirmation message box
    function confirm_window(msg, fun = null, item = null, heading = null) {
        if (msg == null || msg == 'undefined') {
            msg = "Are you sure want to do this?";
        }
        if (heading != null && heading != 'undefined') {
            $('.confirm-box-heading').html('<h4>' + heading + '</h4>');
        }
        $('.confirm-box-msg').html('<p>' + msg + '</p>');
        var confirm_box = $('.confirm-box').bPopup({
            closeClass: 'confirm-box-close',
            escClose: false,
            modalClose: false
        });
        $('.confirm-box-button-yes').on("click", function (e) {
            confirm_box.close();
            if (fun != null && fun != 'undefined') {
                eval(fun)(item);
            }
            return true;
        });
        $('.confirm-box-button-no').on("click", function (e) {
            confirm_box.close();
            return false;
        });
    }
    $('.confirm-click').on("click", function (e) {
        e.preventDefault();
        var msg = $(this).data('msg');
        var heading = $(this).data('heading');
        var fun = $('.confirm-click').data('fun');
        var item = $(this);
        confirm_window(msg, fun, item, heading);
    });
    //loading busy indicator
    $(document).ajaxStart(function () {
        $(".waiting-busy").show();
    });
    $(document).ajaxComplete(function () {
        $(".waiting-busy").hide();
    });
    $('.waitforit').on('click', function (e) {
        $(".waiting-busy").show();
    });
    $('form').submit(function () {
        $(".waiting-busy").show();
    });
    // setting alert message window
    $('.swp-alert-window').hide();
    function alertMessageBar(msg, alertType = 'danger', hide = true, reload = false) {
        if (alertType == 'danger') {
            $('.swp-alert-container').addClass('bg-danger');
        } else if (alertType == 'success') {
            $('.swp-alert-container').addClass('bg-success');
        } else if (alertType == 'warning') {
            $('.swp-alert-container').addClass('bg-warning');
        } else if (alertType == 'primary') {
            $('.swp-alert-container').addClass('bg-primary');
        } else if (alertType == 'inverse') {
            $('.swp-alert-container').addClass('bg-inverse');
        }
        $('.swp-alert-msg').html(msg);
        if (hide = true) {
            $('.swp-alert-window').show(0).delay(5000).hide(0);
        } else {
            $('.swp-alert-window').show(0);
        }
        if (reload == true)
            setTimeout(function () {
                location.reload();
            }, 1200);
    }
    $('.swp-alert-window-close').on('click', function (e) {
        $('.swp-alert-window').hide();
        $('.swp-alert-msg').html('');
    });
    // setting functionality for dragging items using sortable js
    var oldHContainer;
    // setting functionality for dragging header items  
    var menuHGroup = $(".menu-header-drag").sortable({
        group: 'menulist',
        afterMove: function (placeholder, container) {
            if (oldHContainer != container) {
                if (oldHContainer)
                    oldHContainer.el.removeClass("active");
                container.el.addClass("active");
                oldHContainer = container;
            }
        },
        onDrop: function ($item, container, _super) {
            container.el.removeClass("active");
            _super($item, container);
        },
        tolerance: 6,
        distance: 10,
        vertical: false
    });
    menuHGroup.sortable('disable');
    // setting functionality for dragging footer items       
    var menuFGroup = $(".menu-footer-drag").sortable({
        group: 'menulist',
        afterMove: function (placeholder, container) {
            if (oldFContainer != container) {
                if (oldFContainer)
                    oldFContainer.el.removeClass("active");
                container.el.addClass("active");
                oldFContainer = container;
            }
        },
        onDrop: function ($item, container, _super) {
            container.el.removeClass("active");
            _super($item, container);
        },
        tolerance: 6,
        distance: 10,
        vertical: false
    });
    menuFGroup.sortable('disable');
    // setting for header dragging on or off
    $(".menuHeaderDragSwitch").on("click", ".menuHeaderDragSwitch", function (e) {
        var method = $(this).is(":checked") ? "enable" : "disable";
        menuHGroup.sortable(method);
    });
    // setting for footer dragging on or off
    $(".menuFooterDragSwitch").on("click", ".menuFooterDragSwitch", function (e) {
        var method = $(this).is(":checked") ? "enable" : "disable";
        menuFGroup.sortable(method);
    });
    // saving menu header items 
    $('.saveHeaderMenu').on("click", function (e) {
        var menuData = menuHGroup.sortable("serialize").get();
        var dataSend = {menuData: menuData, swp: swp.token_hash};
        $(this).prop('disabled', true);
        $.ajax({
            type: "POST",
            url: swp.base_url + "dashboard/saveHeaderMenu",
            data: dataSend,
            success: function (result) {
                if (result == true) {
                    alertMessageBar('Header menu item saved successfully. Reloading....', 'success', true, true);
                }
            },
            complete: function () {
                $('.saveHeaderMenu').prop('disabled', false);
            }
        });
    });
    // saving menu footer items 
    $('.saveFooterMenu').on("click", function (e) {
        var menuData = menuFGroup.sortable("serialize").get();
        var dataSend = {menuData: menuData, swp: swp.token_hash};
        $(this).prop('disabled', true);
        $.ajax({
            type: "POST",
            url: swp.base_url + "dashboard/saveFooterMenu",
            data: dataSend,
            success: function (result) {
                if (result == true) {
                    alertMessageBar('Footer menu item saved successfully. Reloading....', 'success', true, true);
                }
            },
            complete: function () {
                $('.saveFooterMenu').prop('disabled', false);
            }
        });
    });
    // delete site logo     
    function siteLogo(e) {
        e.prop('disabled', true);
        $.ajax({
            url: swp.base_url + "dashboard/deleteLogo",
            success: function (result) {
                if (result == true) {
                    location.reload();
                }
            },
            complete: function () {
                e.prop('disabled', false);
            }

        });
    }
    // delete site dashboard logo     
    function siteDasboardLogo(e) {
        e.prop('disabled', true);
        $.ajax({
            url: swp.base_url + "dashboard/deleteDashboardLogo",
            success: function (result) {
                if (result == true) {
                    location.reload();
                }
            },
            complete: function () {
                e.prop('disabled', false);
            }
        });
    }
    // delete site dashboard logo     
    function siteFavicon(e) {
        e.prop('disabled', true);
        $.ajax({
            url: swp.base_url + "dashboard/deleteFavicon",
            success: function (result) {
                if (result == true) {
                    location.reload();
                }
            },
            complete: function () {
                e.prop('disabled', false);
            }

        });
    }
    //create album
    $('.create-album-button').on('click', function () {
        $(".create-album-alert").html('');
        var createAlbum = $('.create-album').bPopup({
            closeClass: 'create-album-close',
            escClose: false,
            modalClose: false
        });
        $('.create-album-close').on("click", function (e) {
            createAlbum.close();
            location.reload();
        });
    })
    $('#create-album-button').on("click", function (e) {
        var albumName = $("#createAlbum").val();
        var dataSend = {albumName: albumName, swp: swp.token_hash};
        $(this).prop('disabled', true);
        $.ajax({
            type: "POST",
            url: swp.base_url + "dashboard/createAlbum",
            data: dataSend,
            success: function (result) {
                if (result == true) {
                    $("#createAlbum").val('');
                    $(".create-album-alert").html(`<div class="alert alert-success alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>Album created successfully</div>`);
                } else {
                    $(".create-album-alert").html(`<div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>` + result + `</div>`);
                }
            },
            complete: function () {
                $('#create-album-button').prop('disabled', false);
            }
        });
    });
    // album upload images
    $('.upload-album-button').on('click', function () {
        $('.album-upload-status').html('');
        var albumUpload = $('.album-upload').bPopup({
            closeClass: 'album-upload-close',
            escClose: false,
            modalClose: false
        });
        $('.album-upload-close').on("click", function (e) {
            albumUpload.close();
            $('#album-upload-progress').hide();
            location.reload();
        });
    });
    $('#album-upload-progress').hide();
    $('#upload-album-button').on('click', function () {
        var selectAlbum = $('#selectAlbum').val();
        var selectedAlbumCount = selectAlbum.length;
        var fileCount = $('#albumPhotoUpload')[0].files.length;
        $('.uploaded-file-status-content').html('');
        if (selectedAlbumCount == 0) {
            $('.album-upload-status').html('<span class="text-danger">Album name required.<span>');
        } else if (fileCount > 0 && selectedAlbumCount > 0) {
            $('#selectAlbum').prop('disabled', true);
            $('#upload-album-button').prop('disabled', true);
            var uploadedFileCount = 0;
            $.each($('#albumPhotoUpload')[0].files, function (index, item) {
                var formData = new FormData();
                var file_index = item;
                formData.append('albumUploadFile', file_index);
                formData.append('selectAlbum', selectAlbum);
                formData.append('swp', swp.token_hash);
                $.ajax({
                    xhr: function () {
                        var xhr = new window.XMLHttpRequest();
                        xhr.upload.addEventListener("progress", function (e) {
                            if (e.lengthComputable) {
                                $('#album-upload-progress').show();
                                var percentComplete = e.loaded / e.total;
                                percentComplete = parseInt(percentComplete * 100);
                                $('#album-upload-progress').css({
                                    width: percentComplete + "%"
                                });
                                $('#album-upload-progress').attr({
                                    "aria-valuenow": percentComplete
                                })
                                $('#album-upload-progress').html(percentComplete + "%");
                                if (percentComplete === 100) {
                                    $('#album-upload-progress').html('');
                                    $('#album-upload-progress').css({
                                        width: 0
                                    });
                                    $('#album-upload-progress').attr({
                                        "aria-valuenow": 0
                                    })
                                    $('#album-upload-progress').hide();
                                }
                            }
                        }, false);
                        return xhr;
                    },
                    url: swp.base_url + 'dashboard/albumImageUpload',
                    type: 'POST',
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        if (data) {
                            console.log(data)
                            data = '<p class="text-danger">' + data + '</p>';
                            $('.uploaded-file-status-content').append(data);
                        } else {
                            uploadedFileCount++;
                        }
                        $('.album-upload-status').html('<p class="badge badge-pill badge-warning text-gray-dark">' + uploadedFileCount + ' out of ' + fileCount + ' file has been uploaded.</p>');
                    },
                    complete: function () {
                        $('#albumPhotoUpload').filestyle('clear');
                    }
                });
                if (fileCount == (index + 1)) {
                    $('#upload-album-button').prop('disabled', false);
                    $('#selectAlbum').prop('disabled', false);
                }
            });
        } else {
            $('.album-upload-status').html('<span class="text-danger">No file selected.</span>');
        }
    });
    // album using nano gallery plugin   
    if ($('#swpAlbum').length > 0) {
        // getting albums and images
        var gallery_image = $('#swpAlbum').data('gallery');
        gallery_image = gallery_image.replace(/'/g, '"');
        gallery_image = '[' + gallery_image + ']';
        var album = $.parseJSON(gallery_image);
        $("#swpAlbum").nanogallery2({
            items: $.parseJSON(gallery_image),
            thumbnailWidth: 300,
            thumbnailAlignment: "left",
            thumbnailBorderVertical: 1,
            thumbnailBorderHorizontal: 1,
            //thumbnailOpenImage: false, // disable image viewer
            galleryDisplayMode: "moreButton",
            galleryDisplayMoreStep: 5,
            galleryTheme: {
                thumbnail: {
                    background: "rgba(68,68,68,1)",
                    borderColor: 'rgba(0,0,0,1)'
                },
            },
            colorScheme: 'dark',
            thumbnailLabel: {
                display: false
            },
            thumbnailHoverEffect2: "imageScaleIn80",
            fnThumbnailInit: customIcon,
            fnThumbnailToolCustAction: customEvent
        });
    }
    // click/touch on custom tool/button
    function customEvent(action, item) {
        switch (action) {
            case 'album_edit':
                editAlbum(item);
                break;
            case 'album_delete':
                var msg = "Are you sure want to delete this album?";
                var fun = "delete_album";
                confirm_window(msg, fun, item);
                break;
            case 'image_edit':
                var msg = "Are you sure want to delete this image?";
                confirm_window(msg);
                break;
            case 'image_delete':
                var msg = "Are you sure want to delete this image?";
                var fun = "delete_album_image";
                confirm_window(msg, fun, item);
                break;
        }
    }
    // Add custom elements after one thumbnail is build
    function customIcon($e, item, GOMidx) {
        if (item.kind == "album") {
            $e.find('.nGY2GThumbnailSub').append('<i class="ngy2info fa fa-edit album-icon" title="Edit album" data-ngy2action="album_edit"></i><i class="ngy2info fa fa-trash album-icon ml-4" title="Delete album" data-ngy2action="album_delete"></i>');
        } else {
            $e.find('.nGY2GThumbnailSub').append('<i class="ngy2info fa fa-edit album-icon" title="Edit image" data-ngy2action="image_edit"></i><i class="ngy2info fa fa-trash album-icon ml-4" title="Delete image" data-ngy2action="image_delete"></i>');
        }
    }
    function editAlbum(item) {
        $(".update-album-alert").html('');
        $('#updateAlbum').attr('data-album-id', item.GetID());
        $('#updateAlbum').val(item.title);
        var updateAlbum = $('.update-album').bPopup({
            closeClass: 'update-album-close',
            escClose: false,
            modalClose: false
        });
        $('.update-album-close').on("click", function (e) {
            $('#updateAlbum').attr('data-album-id', '');
            $('#updateAlbum').val('');
            updateAlbum.close();
            location.reload();
        });
    }
    function delete_album(e) {
        var dataSend = {albumId: e.GetID(), swp: swp.token_hash};
        $.ajax({
            type: "POST",
            url: swp.base_url + "dashboard/deleteAlbum",
            data: dataSend,
            success: function (result) {
                location.reload();
            },
            complete: function () {
            }
        });
    }
    function delete_album_image(e) {
        var dataSend = {imageId: e.GetID(), swp: swp.token_hash};
        $.ajax({
            type: "POST",
            url: swp.base_url + "dashboard/deleteGalleryImage",
            data: dataSend,
            success: function (result) {               
                location.reload();
            },
            complete: function () {
            }
        });
    }
    // $("#swpAlbum").nanogallery2('refresh');
});
