<?php
class User_Model extends SWP_Model {
    protected $tableName = 'swp_user';
    function __construct() {
        parent::__construct($this->tableName);
    }
    public function userExists() {
        $where = array('user_type' => 'A');
        return $this->countTotalRows($where);
    }
    public function register() {
        $data = array(
            'username' => $this->input->post('rUsername'),
            'user_email' => $this->input->post('rEmail'),
            'user_password' => $this->hash($this->input->post('rPassword')),
            'user_type' => 'A'
        );        
        return $this->insert($data);
    }
    public function login() {
        $data = array(
            'usernameEmail' => $this->input->post('lUsernameEmail'),
            'userpassword' => $this->hash($this->input->post('lPassword'))
        );        
        $where = "(username ='" . $data['usernameEmail'] . "'
            or user_email = '" . $data['usernameEmail'] . "')
            and user_password = '" . $data['userpassword'] . "'
            and user_type= 'A'";
        $column = array('user_id', 'username', 'user_type');
        return $this->select($column, $where, TRUE);
    }
    public function hash($string) {
        return hash('sha512', $string . config_item('encryption_key'));
    }
    public function logout() {
        $this->session->sess_destroy();
    }
}