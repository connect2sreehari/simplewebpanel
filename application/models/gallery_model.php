<?php
class Gallery_Model extends SWP_Model {
    protected $tableName = 'swp_gallery';
    function __construct() {
        parent::__construct($this->tableName);
    }
    // getting gallery images
    public function getGallery($album_id = NULL) {
        if ($album_id != NULL) {
            $where = array('swp_album_album_id' => $album_id);
            $fetch = FALSE;
        } else {
            $fetch = FALSE;
            $where = FALSE;
        }
        $order_by = 'added_on DESC';
        return $this->select('*', $where, $fetch, $order_by);
    }
    // getting gallery image
    public function getGalleryImage($image_id) {
        $where = array('image_id' => $image_id);
        return $this->select('*', $where, TRUE);
    }
    // inserting gallery image data
    public function createGalleryImage($image) {
        $data = [];
        if (!empty($image['album_id'])) {
            $data['swp_album_album_id'] = $image['album_id'];
        }
        if (!empty($image['title'])) {
            $data['image_title'] = $image['title'];
        }
        if (!empty($image['description'])) {
            $data['image_description'] = $image['description'];
        }
        if (!empty($image['path'])) {
            $data['image_path'] = $image['path'];
        }
        if (!empty($image['thumb_path'])) {
            $data['image_thumb_path'] = $image['thumb_path'];
        }
        return $this->insert($data);
    }
    // deleting gallery image
    public function deleteGalleryImage($image_id) {
        $where = array('image_id' => $image_id);
        return $this->delete($where);
    }
    // delete gallery images
    public function deleteGallery($album_id = NULL) {
        $where = array('swp_album_album_id' => $album_id);
        return $this->delete($where, FALSE);
    }
}