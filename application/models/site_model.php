<?php
class Site_Model extends SWP_Model {
    protected $tableName = 'swp_site';
    function __construct() {
        parent::__construct($this->tableName);
    }
    public function siteInfoInsert($logo_path = NULL, $dashboard_logo_path = NULL, $favicon_path = NULL) {
        $data = array(
            'site_name' => $this->input->post('siteTitle'),
            'site_meta_tag' => $this->input->post('siteMetaTag')
        );
        if ($logo_path != NULL) {
            $data['site_logo'] = $logo_path;
        }
        if ($dashboard_logo_path != NULL) {
            $data['site_logo_dashboard'] = $dashboard_logo_path;
        }
        if ($favicon_path != NULL) {
            $data['site_favicon'] = $favicon_path;
        }        
        $where = array('site_id' => '1');
        if ($this->insert($data, $where)) {
            return TRUE;
        } else {
            if ($this->insert($data)) {
                return TRUE;
            }
        }
        return FALSE;
    }
    public function getSiteInfo() {
        if ($this->tableExists()) {
            $this->delete(array('site_id!=' => '1'));
            $where = array('site_id' => '1');
            return $this->select("*", $where, TRUE);
        }
    }
    public function deleteLogo() {
        $data = array('site_logo' => '');
        $where = array('site_id' => '1');
        if ($this->insert($data, $where)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    public function deleteDashboardLogo() {
        $data = array('site_logo_dashboard' => '');
        $where = array('site_id' => '1');
        if ($this->insert($data, $where)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    public function deleteFavicon() {
        $data = array('site_favicon' => '');
        $where = array('site_id' => '1');
        if ($this->insert($data, $where)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
}