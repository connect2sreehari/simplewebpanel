<?php
class Menu_Model extends SWP_Model {
    protected $tableName = 'swp_menu';
    function __construct() {
        parent::__construct($this->tableName);
    }
    // getting the menu items 
    public function getAllMenu($menu_id = NULL) {
        $order_by = 'menu_parent_id ASC';
        if ($menu_id != NULL) {
            $where = array('menu_id' => $menu_id);
            $fetch = TRUE;
        } else {
            $where = FALSE;
            $fetch = FALSE;
        }
        return $this->select('*', $where, $fetch, $order_by);
    }
    // getting all the header menu item based on parent id
    public function getHeaderMenu($parent_id = 0) {
        $order_by = 'menu_header_order ASC';
        $where = array('menu_parent_id' => $parent_id, 'menu_show_in!=' => 'F');
        return $this->select('*', $where, FALSE, $order_by);
    }
    // getting all the footer menu item based on parent id
    public function getFooterMenu($parent_id = 0) {
        $order_by = 'menu_footer_order ASC';
        $where = array('menu_parent_id' => $parent_id, 'menu_show_in!=' => 'H');
        return $this->select('*', $where, FALSE, $order_by);
    }
    // inserting  menu item and setting order number
    public function insertMenuItem() {
        $parent_id = $this->input->post('menuParentItem') ? $this->input->post('menuParentItem') : 0;
        $menu_header_order = count($this->getHeaderMenu($parent_id)) ? count($this->getHeaderMenu($parent_id)) + 1 : 1;
        $menuHeaderFooterSwitch = $this->input->post('menuHeaderFooterSwitch[]');
        $this->load->helper('menu');
        $menu_switch = menuHFSwitch($menuHeaderFooterSwitch);
        if ($menu_switch != 'H') {
            $menu_footer_order = count($this->getFooterMenu($parent_id)) ? count($this->getFooterMenu($parent_id)) + 1 : 1;
        } else {
            $menu_footer_order = 0;
        }
        $data = array(
            'menu_name' => $this->input->post('menuItemName'),
            'menu_parent_id' => $parent_id,
            'menu_display_name' => $this->input->post('menuDisplayName'),
            'menu_link' => filter_var($this->input->post('menuItemLink'), FILTER_VALIDATE_URL) ? $this->input->post('menuItemLink') : $this->input->post('menuItemLink') == '#' ? '#' : base_url($this->input->post('menuItemLink')),
            'menu_header_order' => $menu_header_order,
            'menu_show_in' => $menu_switch,
            'menu_footer_order' => $menu_footer_order
        );
        if (!empty($this->input->post('menuEdit'))) {
            $menu_edit_id = $this->input->post('menuEdit');
            $where = array('menu_id' => $menu_edit_id);
        } else {
            $where = FALSE;
        }
        if ($this->insert($data, $where)) {
            return TRUE;
        }
        return FALSE;
    }
    // getting the header menu parent child
    public function getHeaderChild($menu_id) {
        $result = [];
        $record = $this->getHeaderMenu($menu_id);
        foreach ($record as $row) {
            $result += array($row->menu_id => array('menu_id' => $row->menu_id, 'menu_name' => $row->menu_name, 'menu_display_name' => $row->menu_display_name, 'menu_link' => $row->menu_link));
            if ($row->menu_parent_id != 0) {
                $child = $this->getHeaderChild($row->menu_id);
                if ($child) {
                    $result[$row->menu_id] += array('child' => $child);
                }
            }
        }
        return $result ? $result : '';
    }
    // getting the footer menu parent child
    public function getFooterChild($menu_id) {
        $result = [];
        $record = $this->getFooterMenu($menu_id);
        foreach ($record as $row) {
            $result += array($row->menu_id => array('menu_id' => $row->menu_id, 'menu_name' => $row->menu_name, 'menu_display_name' => $row->menu_display_name, 'menu_link' => $row->menu_link));
            if ($row->menu_parent_id != 0) {
                $child = $this->getFooterChild($row->menu_id);
                if ($child) {
                    $result[$row->menu_id] += array('child' => $child);
                }
            }
        }
        return $result ? $result : '';
    }
    // updating menu order and deleting items that are removed from save
    public function updateMenuOrder($menuOrder = NULL, $menuFor = 'H') {
        $menu_id_list = '';
        $i = 0;
        if (count($menuOrder)) {
            foreach ($menuOrder as $menuId => $menuItem) {
                $menu_id_list .= $i != 0 ? ',' : '';
                $menu_id_list .= $menuId;
                $data = array(
                    'menu_parent_id' => $menuItem['parent'] ?: 0
                );
                $where = array('menu_id' => $menuId);
                if ($menuFor == 'H') {
                    $data['menu_header_order'] = $menuItem['order'];
                    $where['menu_show_in!='] = 'F';
                } else if ($menuFor == 'F') {
                    $data['menu_footer_order'] = $menuItem['order'];
                    $where['menu_show_in!='] = 'H';
                }
                $this->insert($data, $where);
                $i++;
            }
        }
        if ($menuFor == 'H') {
            $data = array(
                'menu_show_in' => 'F'
            );
            $where = "menu_show_in='HF'";
            $where .= $menu_id_list ? " and menu_id not in ($menu_id_list)" : '';
            $this->insert($data, $where);
            $where = "menu_show_in!='F'";
            $where .= $menu_id_list ? " and menu_id not in ($menu_id_list)" : '';
            $this->delete($where, FALSE);
        } else if ($menuFor == 'F') {
            $data = array(
                'menu_show_in' => 'H'
            );
            $where = "menu_show_in='HF'";
            $where .= $menu_id_list ? " and menu_id not in ($menu_id_list)" : '';
            $this->insert($data, $where);
            $where = "menu_show_in!='H'";
            $where .= $menu_id_list ? " and menu_id not in ($menu_id_list)" : '';
            $this->delete($where, FALSE);
        }
    }
    // checking row exists
    public function checkRowExists($where = NULL, $column = '*') {
        if ($where == NULL) {
            return FALSE;
        }
        if ($this->countTotalRows($where, $column) > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    // setting menu items of header and footer to display
    public function getParentMenuItems($records = NULL, $for) {
        $menu_list = [];
        foreach ($records as $record) {
            $menu_id = $record->menu_id;
            $menu_name = $record->menu_name;
            $menu_display_name = $record->menu_display_name;
            $menu_link = $record->menu_link;
            if ($for == 'H') {
                $menu_list[$menu_id] = array('menu_id' => $menu_id, 'menu_name' => $menu_name, 'menu_display_name' => $menu_display_name, 'menu_link' => $menu_link);
                $child = $this->getHeaderChild($menu_id);
                if (!empty($child)) {
                    $menu_list[$menu_id] += array('child' => $child);
                }
            } else if ($for == 'F') {
                $menu_list[$menu_id] = array('menu_id' => $menu_id, 'menu_name' => $menu_name, 'menu_display_name' => $menu_display_name, 'menu_link' => $menu_link);
                $child = $this->getFooterChild($menu_id);
                if (!empty($child)) {
                    $menu_list[$menu_id] += array('child' => $child);
                }
            }
        }
        return $menu_list;
    }
}