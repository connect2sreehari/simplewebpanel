<?php
class Album_Model extends SWP_Model {
    protected $tableName = 'swp_album';
    function __construct() {
        parent::__construct($this->tableName);
    }
    //checking album name exists
    public function albumNameExists($name) {
        $where = array('album_name' => $name);
        return $this->countTotalRows($where);
    }
    // getting all the album 
    public function getAlbum($album_id = NULL) {
        if ($album_id != NULL) {
            $where = array('album_id' => $album_id);
            $fetch = TRUE;
        } else {
            $fetch = FALSE;
            $where = FALSE;
        }
        $order_by = 'album_name ASC';
        return $this->select('*', $where, $fetch, $order_by);
    }
    // creating new album
    public function createAlbum($album_id = NULL) {
        if ($album_id != NULL) {
            $where = array('album_id' => $album_id);
        } else {
            $where = FALSE;
        }
        $data = array(
            'album_name' => ucfirst($this->input->post('albumName'))
        );
        return $this->insert($data, $where);
    }
    public function deleteAlbum($album_id) {
        $where = array('album_id' => $album_id);
        return $this->delete($where);
    }
}