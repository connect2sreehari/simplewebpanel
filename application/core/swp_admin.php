<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class SWP_Admin extends SWP_Controller {
    function __construct() {
        parent::__construct();        
        $this->data['segment'] = array('controller' => '', 'method' => '');
        $this->data['logged_in'] = $this->session->logged_in;
        $this->data['site_content'] = '';
        $this->checkUser();
    }
    // alert box of core ui template for error message based on sesssion flashdata of codeigniter
    public function alertMessage() {
        if ($this->session->flashdata('error')) {
            return '<div class="swp-alert alert alert-danger alert-dismissible fade show" role="alert">
                 <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                <strong>Error!</strong> ' . $this->session->flashdata('error') . ' 
              </div>';
        } else if ($this->session->flashdata('success')) {
            return '<div class="swp-alert alert alert-success alert-dismissible fade show" role="alert">
                 <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                 </button>
                <strong>Success!</strong> ' . $this->session->flashdata('success') . '
              </div>';
        } else if ($this->session->flashdata('info')) {
            return '<div class="swp-alert alert alert-info alert-dismissible fade show" role="alert">
                 <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                 </button>
                <strong>Info!</strong> ' . $this->session->flashdata('info') . '
              </div>';
        } else if ($this->session->flashdata('warning')) {
            return '<div class="swp-alert alert alert-warning alert-dismissible fade show" role="alert">
                 <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                 </button>
                <strong>Warning!</strong> ' . $this->session->flashdata('warning') . '
              </div>';
        }
    }
    // checking user logged_in redirecting based on it, storing controller name and method name  
    private function checkUser() {
        $this->data['segment']['controller'] = $this->uri->segment(1) ? $this->uri->segment(1) : '';
        $this->data['segment']['method'] = $this->uri->segment(2) ? $this->uri->segment(2) : '';
        if ($this->session->logged_in == TRUE) {
            if ($this->data['segment']['controller'] != 'dashboard') {
                redirect('dashboard');
            }
        } else if ($this->data['segment']['controller'] != 'admin') {
            redirect('admin/login');
        }
    }
    // calling codeigniter migration  
    protected function migration($migration_no = 0) {
        $this->load->library('migration');
        $this->migration->version($migration_no);
    }
}