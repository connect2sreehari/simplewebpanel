<?php
defined('BASEPATH') OR exit('No direct script access allowed');
Class SWP_Model extends CI_Model {
    protected $tableName = NULL;
    function __construct($table_name) {
        parent::__construct();
        $this->tableName = $table_name;
    }
    // calling select query based on parameters
    public function select($column = '*', $where = FALSE, $fetch_row = FALSE, $order_by = FALSE) {
        $this->db->select($column);
        if ($where) {
            $this->security->xss_clean($where);
            $this->db->where($where);
        } if ($fetch_row == TRUE) {
            $method = "row";
        } else {
            $method = "result";
        }
        if ($order_by) {
            $this->db->order_by($order_by);
        }
        return $this->db->get($this->tableName)->$method();
    }
    // calling insert or update query based on parameter
    public function insert($data, $where = FALSE) {
        //for insert
        if ($where == FALSE) {
            $this->security->xss_clean($data);
            $this->db->set($data);
            $this->db->insert($this->tableName);
            return $this->db->insert_id();
        }
        //for update
        else {
            $this->security->xss_clean($data);
            $this->security->xss_clean($where);
            $this->db->set($data);
            $this->db->where($where);
            $this->db->update($this->tableName);
            return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
        }
        return FALSE;
    }
    // calling delete query based on parameter
    public function delete($where = FALSE, $limit = TRUE) {
        if ($where == FALSE) {
            return FALSE;
        }
        $this->db->where($where);
        $this->security->xss_clean($where);
        if ($limit == TRUE)
            $this->db->limit(1);
        $this->db->delete($this->tableName);
        return TRUE;
    }
    // checking table exits or note
    public function tableExists() {
        return $this->db->table_exists($this->tableName);
    }
    // get count of all the rows in table
    public function countTotalRows($where = NULL, $column = '*') {
        if ($where != NULL) {
            $this->security->xss_clean($where);
            $this->db->where($where);
        }
        $this->db->select($column);
        return $this->db->count_all_results($this->tableName);
    }
}