<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class SWP_Controller extends CI_Controller {
    // declaring public variable array for sending data to view 
    public $data = array();
    // setting default values in the data variable
    function __construct() {
        parent::__construct();
        $this->data['error'] = array();
        $this->data['alert'] = '';
        $this->data['site_name'] = '';
        $this->__loadData();
    }
    // loading default data from model
    private function __loadData() {
        $this->data['swp'] = array(
            'dashboard_logo' => '',
            'logo' => '',
            'site_name' => config_item('swp_sitename'),
            'metatag' => ''
        );
        $this->load->model('site_model');
        $site_data = $this->site_model->getsiteinfo();
        if (count($site_data)) {
            $data = & $this->data['swp'];
            $data['dashboard_logo'] = $site_data->site_logo_dashboard;
            $data['logo'] = $site_data->site_logo;
            $data['site_name'] = $site_data->site_name;
            $data['meta_tag'] = $site_data->site_meta_tag;
        }
    }
}