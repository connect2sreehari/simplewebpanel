<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_SWP_Gallery extends CI_Migration {
    public function up() {
        $this->dbforge->add_field(array(
            'image_id' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'auto_increment' => TRUE,
                'null' => FALSE
            ),
            'image_title' => array(
                'type' => 'VARCHAR',
                'constraint' => 500
            ),
            'image_description' => array(
                'type' => 'VARCHAR',
                'constraint' => 1500
            ),
            'image_path' => array(
                'type' => 'VARCHAR',
                'constraint' => 500,
                'null' => FALSE
            ),
            'image_thumb_path' => array(
                'type' => 'VARCHAR',
                'constraint' => 500,
                'null' => FALSE
            ),
            'swp_album_album_id' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'null' => FALSE
            ),
            'added_on' => array(
                'type' => 'TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'
            ))
        );
        $this->dbforge->add_key('image_id', TRUE);
        $this->dbforge->add_key('swp_album_album_id');
        $this->dbforge->create_table('swp_gallery');
    }
    public function down() {
        $this->dbforge->drop_table('swp_gallery');
    }
}