<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_SWP_Article extends CI_Migration {
    public function up() {
        $this->dbforge->add_field(array(
            'article_id' => array(
                'type' => 'INT',                
                'unsigned' => TRUE,
                'auto_increment' => TRUE,
                'null' => FALSE
            ),
            'article_title' => array(
                'type' => 'VARCHAR',
                'constraint' => 500,
                'null' => FALSE
            ),
            'article_description' => array(
                'type' => 'TEXT',
                'null' => FALSE              
            ),
            'swp_menu_menu_id' => array(
                'type' => 'INT',
                'unsigned' => TRUE,                
                'null' => FALSE
            ),
            'added_on' => array(
                'type' => 'TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',       
            ))
        );
        $this->dbforge->add_key('article_id', TRUE);
        $this->dbforge->add_key('swp_menu_menu_id');
        $this->dbforge->create_table('swp_article');
    }
    public function down() {
        $this->dbforge->drop_table('swp_article');
    }
}