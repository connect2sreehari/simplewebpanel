<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_SWP_Site extends CI_Migration {
    public function up() {
        $this->dbforge->add_field(array(
            'site_id' => array(
                'type' => 'TINYINT',
                'constraint' => 1,
                'unsigned' => TRUE,
                'auto_increment' => TRUE,
                'null' => FALSE
            ),
            'site_name' => array(
                'type' => 'VARCHAR',
                'constraint' => 100,
                'null' => FALSE
            ),
            'site_logo' => array(
                'type' => 'VARCHAR',
                'constraint' => 255                
            ),
            'site_logo_dashboard' => array(
                'type' => 'VARCHAR',
                'constraint' => 255                
            ),
            'site_favicon' => array(
                'type' => 'VARCHAR',
                'constraint' => 255                
            ),
            'site_meta_tag' => array(
                'type' => 'TEXT',
                'null' => FALSE
            ))
        );
        $this->dbforge->add_key('site_id', TRUE);
        $this->dbforge->create_table('swp_site');
    }
    public function down() {
        $this->dbforge->drop_table('swp_site');
    }
}