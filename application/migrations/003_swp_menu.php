<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_SWP_Menu extends CI_Migration {
    public function up() {
        $this->dbforge->add_field(array(
            'menu_id' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'auto_increment' => TRUE,
                'null' => FALSE
            ),
            'menu_name' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'unique' => TRUE,
                'null' => FALSE
            ),
            'menu_display_name' => array(
                'type' => 'VARCHAR',
                'constraint' => 255,
                'null' => FALSE
            ),
            'menu_parent_id' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
                'default' => '0'
            ),
            'menu_link' => array(
                'type' => 'TEXT',                
                'null' => FALSE
            ),
            'menu_show_in' => array(
                'type' => 'ENUM("H","F","HF")',
                'default' => 'H'                
            ),
            'menu_header_order' => array(
                'type' => 'INT'                              
            ),
            'menu_footer_order' => array(
                'type' => 'INT'               
            ))                
        );
        $this->dbforge->add_key('menu_id', TRUE);
        $this->dbforge->create_table('swp_menu');
    }
    public function down() {
        $this->dbforge->drop_table('swp_menu');
    }
}