<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_SWP_User extends CI_Migration {
    public function up() {
        $this->dbforge->add_field(array(
            'user_id' => array(
                'type' => 'INT',
                'constraint' => 2,
                'unsigned' => TRUE,
                'auto_increment' => TRUE,
                'null' => FALSE
            ),
            'username' => array(
                'type' => 'VARCHAR',
                'constraint' => 15,
                'unique' => TRUE,
                'null' => FALSE
            ),
            'user_email' => array(
                'type' => 'NVARCHAR',
                'constraint' => 255,
                'unique' => TRUE,
                'null' => FALSE
            ),
            'user_password' => array(
                'type' => 'VARCHAR',
                'constraint' => 128,
                'null' => FALSE
            ),
            'user_type' => array(
                'type' => 'ENUM("A","U")',
                'default' => 'A',
                'null' => FALSE
            ))
        );
        $this->dbforge->add_key('user_id', TRUE);
        $this->dbforge->create_table('swp_user');
    }
    public function down() {
        $this->dbforge->drop_table('swp_user');
    }
}