<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_SWP_Album extends CI_Migration {
    public function up() {
        $this->dbforge->add_field(array(
            'album_id' => array(
                'type' => 'INT',                
                'unsigned' => TRUE,
                'auto_increment' => TRUE,
                'null' => FALSE
            ),
            'album_name' => array(
                'type' => 'VARCHAR',
                'constraint' => 250,
                'null' => FALSE
            ))
        );
        $this->dbforge->add_key('album_id', TRUE);
        $this->dbforge->create_table('swp_album');         
    }
    public function down() {
        $this->dbforge->drop_table('swp_album');       
    }
}