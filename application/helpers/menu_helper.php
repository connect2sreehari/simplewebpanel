<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// creating menu drag list 
if (!function_exists('get_menu_drag_list')) {
    function get_menu_drag_list($data = '', $colorIndex = 0) {
        $menuDragList = '';
        $colorMenuWindow = array('bg-inverse text-white', 'bg-success text-white', 'bg-primary text-white', 'bg-danger text-white', 'bg-info text-white', 'bg-warning text-white');
        foreach ($data as $parent) {
            $menuDragList .= '<li class="alert ' . $colorMenuWindow[$colorIndex] . ' alert-dismissible fade show"  role="alert" data-menuId="' . $parent['menu_id'] . '">                                    
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <a href="' . base_url('dashboard/menu/' . $parent['menu_id']) . '" class="menu-edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>' . $parent['menu_display_name'];
            $menuDragList .= '<ol>';
            if (array_key_exists('child', $parent)) {
                $colorIndex++;
                if ($colorIndex == 6) {
                    $colorIndex = 0;
                }
                $menuDragList .= get_menu_drag_list($parent['child'], $colorIndex);
                $colorIndex--;
                if ($colorIndex < 0) {
                    $colorIndex = 0;
                }
            }
            $menuDragList .= '</ol>';
            $menuDragList .= '</li>';
        }
        return $menuDragList;
    }
}
// getting saved menu order
if (!function_exists('getSavedOrder')) {
    function getSavedOrder($result, $parent = 0) {
        $menuOrder = [];
        $i = 1;
        if (count($result)) {
            foreach ($result as $row) {
                if (!empty($row['menuid'])) {
                    $menuOrder[$row['menuid']] = array('parent' => $parent, 'order' => $i);
                }
                if (!empty($row['children'])) {
                    $menuOrder += getSavedOrder($row['children'][0], $row['menuid']);
                }
                $i++;
            }
        }
        return $menuOrder;
    }
}
// checking menu item part of header or footer or both
if (!function_exists('menuHFSwitch')) {
    function menuHFSwitch($menuHeaderSwitch = NULL) {
        if ($menuHeaderSwitch == NULL) {
            return TRUE;
        } else if (in_array('H', $menuHeaderSwitch) && in_array('F', $menuHeaderSwitch)) {
            return 'HF';
        } else if (in_array('H', $menuHeaderSwitch)) {
            return 'H';
        } else if (in_array('F', $menuHeaderSwitch)) {
            return 'F';
        } else {
            return FALSE;
        }
    }
}
