<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// upload single file
if (!function_exists('check_upload_file')) {
    function check_upload_file($file, $config) {
        if (!empty($config['extension'])) {
            $file_extension = pathinfo($file['name'], PATHINFO_EXTENSION);
            if (!in_array($file_extension, $config['extension'])) {
                return 'The filetype you are attempting to upload is not allowed.';
            }
        }
        if (!empty($config['size'])) {
            if ($file['size'] > $config['size']) {
                if (!in_array($file_extension, $config['extension'])) {
                    return "The file you are attempting to upload is too large.";
                }
            }
        }
        $image_info = getimagesize($file['tmp_name']);
        if (!empty($config['width']) && $image_info) {
            if ($config['width'] != $image_info[0]) {
                if (!in_array($file_extension, $config['extension'])) {
                    return "The image you are attempting to upload doesn't fit into the allowed dimensions.";
                }
            }
        }
        if (!empty($config['height']) && $image_info) {
            if ($config['height'] != $image_info[1]) {
                return "The image you are attempting to upload doesn't fit into the allowed dimensions.";
            }
        }      
    }
}