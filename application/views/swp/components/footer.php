<?php if ($logged_in)  ?>
</div>
<footer class="app-footer">   
    <a href="./"><span class="text-danger">Simple</span><span class="text-gray-dark">webpanel</span></a> © <?= date('Y') ?> Sreehari.
    <span class="float-right">Powered by <a href="./"><span class="text-danger">Simple</span><span class="text-gray-dark">webpanel</span></a>
    </span>
</footer>
<div class="confirm-box rounded">
    <div class="confirm-box-container bg-inverse text-white">
        <a href="#" class="confirm-box-close"><i class="fa fa-close"></i></a>
        <div class="confirm-box-content">
            <div class="confirm-box-heading"><i class="fa fa-exclamation-triangle text-danger"></i></div></h1>
            <div class="confirm-box-msg"></div>
            <div class="confirm-box-buttons">
                <button type="button" class="btn btn-success confirm-box-button-yes">Yes</button>
                <button type="button" class="btn btn-secondary confirm-box-button-no">No</button>
            </div>
        </div>
    </div>
</div>
<div class="swp-alert-window">
    <div class="swp-alert-container" >
        <a href="#" class="swp-alert-window-close"><i class="fa fa-close"></i></a>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">                    
                    <span class="swp-alert-msg text-white"></span> 
                </div>
            </div>
        </div>
    </div>
</div>
<div class="waiting-busy">
    <div class="waiting-busy-container">
        <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
    </div>
</div>
<div id="outdated"></div>
<!-- Bootstrap and necessary plugins -->
<script src="<?= base_url('assets/swp/js/jquery.min.js') ?>"></script>
<script src="<?= base_url('assets/swp/js/tether.min.js') ?>"></script>
<script src="<?= base_url('assets/swp/js/bootstrap.min.js') ?>"></script>
<script src="<?= base_url('assets/swp/js/pace.min.js') ?>"></script>
<!-- GenesisUI main scripts -->
<script src="<?= base_url('assets/swp/js/app.js') ?>"></script>
<!-- outdated browser plugin -->
<script src="<?= base_url('assets/swp/js/outdated_browser/outdatedbrowser.min.js') ?>"></script>
<!-- selectize dropdown plugin -->
<script src="<?= base_url('assets/swp/js/selectize.min.js') ?>"></script>
<!-- filestyle plugin for file browse button style  -->
<script src="<?= base_url('assets/swp/js/filestyle.min.js') ?>"></script>
<!-- bPopup plugin for creating pop up window  -->
<script src="<?= base_url('assets/swp/js/bpopup.min.js') ?>"></script>
<!-- sortable plugin for creating sortable list of items functionality like dragging up and down  -->
<script src="<?= base_url('assets/swp/js/sortable.js') ?>"></script>
<!--- nano gallery 2 plugin for creating image gallery -->
<script src="<?= base_url('assets/swp/js/nanogallery2.min.js') ?>"></script>
<!-- ckeditor plugin for wysiwyg html editor -->
<script src="<?= base_url('assets/swp/js/ckeditor/ckeditor.js') ?>"></script>
<!-- custom scripts required by simplewebpanel -->
<script src="<?= base_url('assets/swp/js/main.js') ?>"></script>
</body>
</html>