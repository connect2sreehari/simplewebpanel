<div class="card">
    <div class="card-block">
        <div class="row">
            <div class="col-sm-12">
                <h4 class="card-title mb-0">Album</h4>
                <div class="small text-muted">Create album and upload images</div>
            </div>
            <!--/.col-->                        
        </div>
        <!--/.row-->
        <div class="row">
            <div class="col-sm-12">
                <div class="album-options d-flex justify-content-end">
                    <button type="submit" class="btn btn-success create-album-button">Create Album</button>
                    <button type="submit" class="btn btn-primary upload-album-button">Upload Images</button>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="album content-gap">               
                    <?php if (empty($image_gallery)): ?>
                        <div class = "card text-center">                            
                            <div class = "card-body p-5">
                                <h4 class = "card-title">Album empty</h4>
                                <p class = "card-text">There is no album or there is no image in any created album.</p>                               
                            </div>                            
                        </div>
                    <?php else: ?>
                        <div id="swpAlbum" data-gallery="<?= $image_gallery ?>"></div>  
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="create-album rounded">
    <div class="create-album-container bg-inverse text-white">
        <a href="#" class="create-album-close"><i class="fa fa-close"></i></a>
        <div class="form-group">
            <label class="col-form-label" for="createAlbum">Create album name for upload images</label>
            <input type="text" class="form-control" id="createAlbum" name="createAlbum" aria-describedby="createAlbumHelp" placeholder="Enter album name"> 
            <small id="createAlbumHelp" class="form-text text-muted">Create new album by typing unique name</small>            
        </div>        
        <div class="form-group d-flex justify-content-center">
            <button type="button" class="btn btn-success" id="create-album-button">Create</button>
        </div>
        <div class="create-album-alert"></div>
    </div>
</div>
<div class="update-album rounded">
    <div class="create-album-container bg-inverse text-white">
        <a href="#" class="create-album-close update-album-close"><i class="fa fa-close"></i></a>
        <div class="form-group">
            <label class="col-form-label" for="updateAlbum">Update album name</label>
            <input type="text" class="form-control" id="updateAlbum" data-album-id="" name="updateAlbum"  placeholder="Enter album name">                      
        </div>        
        <div class="form-group d-flex justify-content-center">
            <button type="button" class="btn btn-success" id="update-album-button">Update</button>
        </div>
        <div class="update-album-alert"></div>
    </div>
</div>
<div class="album-upload rounded">
    <div class="album-upload-container bg-inverse text-white">
        <a href="#" class="album-upload-close"><i class="fa fa-close"></i></a>
        <div class="form-group">
            <label class="col-form-label" for="selectAlbum">Select album name for upload</label>           
            <select id="selectAlbum" name="selectAlbum" aria-describedby="selectAlbumHelp" class="swp-dropdown">
                <option selected value="">Select album name</option>
                <?php foreach ($albums as $album): ?>
                    <option value="<?= $album->album_id ?>"><?= $album->album_name ?></option>
                <?php endforeach; ?>   
            </select>   
        </div>
        <div class="form-group">            
            <label class="col-form-label" for="albumPhotoUpload">Choose files</label>
            <input type="file" id="albumPhotoUpload" data-input="false" accept="image/*" name="albumPhotoUpload[]"  multiple="multiple">
        </div>
        <div class="album-file-alert"></div>
        <div class="form-group d-flex justify-content-end">
            <button type="button" class="btn btn-success" id="upload-album-button">Upload</button>
        </div>
        <div class="album-upload-status">            
        </div>
        <div class="progress">
            <div class="progress-bar" id="album-upload-progress" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
        </div>
        <div class="uploaded-file-status">
            <div class="uploaded-file-status-content">
            </div>            
        </div>
    </div>
</div>
