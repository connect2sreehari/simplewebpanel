<div class="card">
    <div class="card-block">
        <div class="row">
            <div class="col-sm-12">
                <h4 class="card-title mb-0">Article</h4>
                <div class="small text-muted">Enter your website details</div>
            </div>
            <!--/.col-->                        
        </div>
        <!--/.row-->
        <div class="row">
            <div class="col-sm-12">
                <div class="site content-gap">
                    <?= form_open_multipart('dashboard/article') ?>
                    <div class="form-group <?= form_error('page') ? 'has-danger' : '' ?>">                            
                        <label class="col-form-label" for="page">Page name</label>
                        <select id="page" name="page" aria-describedby="pageHelp" class="swp-dropdown">
                            <option selected value="">Choose page for article.</option> 
                            <?php foreach ($page_list as $page): ?>
                                <option value="<?= $page->menuid ?>" <?= $page->menuid == set_value('page') ? 'selected' : '' ?>><?= $page->menudisplayname ?></option>
                            <?php endforeach; ?>                                   
                        </select>  
                        <small id="pageHelp" class="form-text text-muted">Create article for each page.</small>
                        <?= form_error('page', '<div class="alert alert-danger" role="alert">', '</div>') ?>
                    </div>
                    <div class="form-group">
                        <label for="articleTitle">Article Title</label>
                        <input type="text" class="form-control" id="siteTitle" name="articleTitle" aria-describedby="articleTitleHelp" value="" placeholder="Enter article title">
                        <small id="articleTitleHelp" class="form-text text-muted">Main title for article.</small>                        
                    </div>
                    <div class="form-group">
                        <label for="articleEditor">Article</label>
                        <textarea  name="articleEditor" id="articleEditor"></textarea>                        
                    </div>             
                    <div class="d-flex justify-content-end content-gap contain-full">
                        <button type="submit" class="btn btn-primary">Save</button>      
                    </div>
                    <?= form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>