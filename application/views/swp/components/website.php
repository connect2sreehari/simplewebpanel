<div class="card">
    <div class="card-block">
        <div class="row">
            <div class="col-sm-12">
                <h4 class="card-title mb-0">Website</h4>
                <div class="small text-muted">Enter your website details</div>
            </div>
            <!--/.col-->                        
        </div>
        <!--/.row-->
        <div class="row">
            <div class="col-sm-12">
                <div class="site content-gap">
                    <?= form_open_multipart('dashboard/website') ?>                    
                    <div class="form-group <?= form_error('siteTitle') ? 'has-danger' : '' ?>">
                        <label for="siteTitle">Site Name</label>
                        <input type="text" class="form-control" id="siteTitle" name="siteTitle" aria-describedby="siteTitleHelp" value="<?= set_value('siteTitle') ?: $site['name']; ?>" placeholder="Enter your site name">
                        <small id="siteTitleHelp" class="form-text text-muted">It can be used to show in title tag or footer area.</small>
                        <?= form_error('siteTitle', '<div class="alert alert-danger" role="alert">', '</div>') ?>
                    </div>
                    <div class="form-group <?= form_error('siteMetadata') ? 'has-danger' : '' ?>">
                        <label for="siteMetaTag">Meta tags</label>
                        <textarea class="form-control" name="siteMetaTag" id="siteMetaTag" rows="8" aria-describedby="siteMetaTagHelp"><?= set_value('siteMetaTag') ?: $site['meta_tag']; ?></textarea>
                        <small id="siteMetaTagHelp" class="form-text text-muted">Describe meta tag for your website.</small>
                    </div> 
                    <div class="form-group">
                        <label class="col-form-label" for="logoUpload">Site logo</label>
                        <input type="file" id="logoUpload" data-input="false" accept="image/*" name="siteLogoUpload" aria-describedby="siteLogoHelp">
                        <small id="siteLogoHelp" class="form-text text-muted">Upload file must be image(gif, png, jpg, jpeg). Maximum file size 10MB.</small>
                        <?= form_error('siteLogoUpload', '<div class="alert alert-danger" role="alert">', '</div>') ?>                         
                    </div>
                    <?php if (!empty($site['logo'])): ?>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <div class="outer-box">
                                    <div class="outer-box-options">
                                        <button type="button" class="close confirm-click" data-fun="siteLogo" title="Delete logo" data-msg="Are you sure want to delete logo?" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="outer-box-content">
                                        <figure class="figure site-info-logo">
                                            <img src="<?= base_url() . $site['logo'] ?>" style="max-width:300px;" class="figure-img img-fluid" alt="<?= $site['name'] ?>">
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="form-group">
                        <label class="col-form-label" for="logoDashboardUpload">Site dashboard logo</label>
                        <input type="file" id="logoDashboardUpload" data-input="false" accept="image/*" name="siteDashboardLogoUpload" aria-describedby="siteLogoDashboardHelp">
                        <small id="siteLogoDashboardHelp" class="form-text text-muted">Upload file must be image(gif, png, jpg, jpeg). Maximum file size 10MB. Maximum width: 130px and height: 50px allowed.</small>
                        <?= form_error('siteDashboardLogoUpload', '<div class="alert alert-danger" role="alert">', '</div>') ?>                         
                    </div>
                    <?php if (!empty($site['dashboard_logo'])): ?>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <div class="outer-box">
                                    <div class="outer-box-options">
                                        <button type="button" class="close confirm-click" data-fun="siteDasboardLogo" title="Delete dashboard logo" data-msg="Are you sure want to delete dashboard logo?" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="outer-box-content">
                                        <figure class="figure site-info-logo">
                                            <img src="<?= base_url() . $site['dashboard_logo'] ?>" style="max-width:300px;" class="figure-img img-fluid" alt="<?= $site['name'] ?>">
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="form-group">
                        <label class="col-form-label" for="faviconUpload">Site favicon</label>
                        <input type="file" id="faviconUpload" data-input="false" accept="image/*" name="siteFaviconUpload" aria-describedby="faviconUploadHelp">
                        <small id="faviconUploadHelp" class="form-text text-muted">Upload file must be .ico file. Maximum file size 10MB.</small>
                        <?= form_error('siteFaviconUpload', '<div class="alert alert-danger" role="alert">', '</div>') ?>                                       
                    </div>
                    <?php if (!empty($site['favicon'])): ?>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <div class="outer-box">
                                    <div class="outer-box-options">
                                        <button type="button" class="close confirm-click" data-fun="siteFavicon" title="Delete favicon" data-msg="Are you sure want to delete favicon?" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="outer-box-content">
                                        <figure class="figure site-info-logo">
                                            <img src="<?= base_url() . $site['favicon'] ?>" style="max-width:300px;" class="figure-img img-fluid" alt="<?= $site['name'] ?>">
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="d-flex justify-content-end ">
                        <button type="submit" class="btn btn-primary">Save</button>          
                    </div>
                    <?= $alert; ?>
                    <?= form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>