<div class="card">
    <div class="card-block">
        <div class="row">
            <div class="col-sm-12">
                <h4 class="card-title mb-0">Menu</h4>
                <div class="small text-muted">Create menu for your website</div>
            </div>                                 
        </div>        
        <div class="row">
            <div class="col-sm-12">
                <div class="menu-list-container content-gap">  
                    <?= form_open('dashboard/menu') ?>
                    <?php if (!empty($menu_edit['menu_edit_id'])): ?>
                        <input type="hidden" name="menuEdit" value="<?= set_value('menuEdit') ?: $menu_edit['menu_edit_id'] ?>">
                    <?php endif; ?>
                    <div class="row">
                        <div class="form-group col-md-6 <?= form_error('menuItemName') ? 'has-danger' : '' ?>">
                            <label class="col-form-label" for="menuItemName">Menu item name</label>
                            <input type="text" class="form-control" id="menuItemName" name="menuItemName" value="<?= set_value('menuItemName') ?: $menu_edit['menu_name']; ?>" placeholder="Menu item name"> 
                            <?= form_error('menuItemName', '<div class="alert alert-danger" role="alert">', '</div>') ?>
                        </div>
                        <div class="form-group col-md-6 <?= form_error('menuParentItem') ? 'has-danger' : '' ?>">                            
                            <label class="col-form-label" for="menuParentItem">Parent menu item name</label>
                            <select id="menuParentItem" name="menuParentItem" aria-describedby="menuParentItemHelp" class="swp-dropdown">
                                <option selected value="">Choose parent for this menu item if any.</option> 
                                <?php foreach ($menu_list as $menuItem): ?>
                                    <option value="<?= $menuItem->menu_id ?>" <?= $menuItem->menu_id == (set_value('menuParentItem') ?: $menu_edit['menu_parent_id']) ? 'selected' : '' ?>><?= $menuItem->menu_display_name ?></option>
                                <?php endforeach; ?>                                   
                            </select>  
                            <small id="menuParentItemHelp" class="form-text text-muted">No need to select if given menu item as no parent menu.</small>
                            <?= form_error('menuParentItem', '<div class="alert alert-danger" role="alert">', '</div>') ?>
                        </div>                      
                    </div>    
                    <div class="form-group <?= form_error('menuDisplayName') ? 'has-danger' : '' ?>">
                        <label class="col-form-label" for="menuDisplayName">Menu display name</label>
                        <input type="text" class="form-control" id="menuDisplayName" name="menuDisplayName" value="<?= set_value('menuDisplayName') ?: $menu_edit['menu_display_name']; ?>" placeholder="Menu display name">
                        <?= form_error('menuDisplayName', '<div class="alert alert-danger" role="alert">', '</div>') ?>
                    </div>
                    <div class="form-group <?= form_error('menuItemLink') ? 'has-danger' : '' ?>">
                        <label class="col-form-label" for="menuItemLink">Menu item link</label>
                        <input type="text" class="form-control" id="menuItemLink" name="menuItemLink" value="<?= set_value('menuItemLink') ?: ($menu_edit['menu_link'] ?: '#'); ?>" placeholder="Set link for this menu item" aria-describedby="menuItemLinkHelp">
                        <small id="menuItemLinkHelp" class="form-text text-muted">If this field left blank menu item name will be the link path.</small>
                        <?= form_error('menuItemLink', '<div class="alert alert-danger" role="alert">', '</div>') ?>
                    </div>  
                    <div class="form-group <?= form_error('menuHeaderFooterSwitch[]') ? 'has-danger' : '' ?>">
                        <div class="row">
                            <div class="col-md-6">
                                <label class="form-check-label">
                                    Show in header                               
                                    <label class="switch switch-text switch-info menuHeaderSwitch">
                                        <input type="checkbox" name="menuHeaderFooterSwitch[]" value="H" class="switch-input menuHeaderSwitch" <?= set_checkbox('menuHeaderFooterSwitch[]', 'H') || $menu_edit['menu_show_in'] == 'H' || $menu_edit['menu_show_in'] == 'HF' ? 'checked' : ''; ?>>
                                        <span class="switch-label" data-on="On" data-off="Off"></span>
                                        <span class="switch-handle"></span>
                                    </label>
                                </label>
                            </div>
                            <div class="col-md-6">
                                <label class="form-check-label">
                                    Show in footer                               
                                    <label class="switch switch-text switch-success menuFooterSwitch">
                                        <input type="checkbox" name="menuHeaderFooterSwitch[]" value="F" class="switch-input menuFooterSwitch" <?= set_checkbox('menuHeaderFooterSwitch[]', 'F') || $menu_edit['menu_show_in'] == 'F' || $menu_edit['menu_show_in'] == 'HF' ? 'checked' : ''; ?>>
                                        <span class="switch-label" data-on="On" data-off="Off"></span>
                                        <span class="switch-handle"></span>
                                    </label>
                                </label>
                            </div>                            
                        </div>   
                        <?php if (!empty($menu_edit['menu_edit_id'])): ?>
                            <small id="menuHeaderFooterSwitchHelp" class="form-text text-muted">If you update the switch it will not change in child menu items</small>
                        <?php endif; ?>
                        <?= form_error('menuHeaderFooterSwitch[]', '<div class="alert alert-danger" role="alert">', '</div>') ?>                       
                    </div>
                    <div class="d-flex justify-content-end content-gap">
                        <button type="submit" class="btn btn-primary"><?= !empty($menu_edit['menu_edit_id']) ? 'Update' : 'Create' ?></button>
                    </div>                    
                    <?= $alert; ?>
                    <?= form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if (!empty($menu_header_list)): ?>
    <div class="card">
        <div class="card-block">
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="card-title mb-0">Header Menu</h4>
                    <div class="small text-muted">Arrange header menu item order.</div>
                </div>                                       
            </div>            
            <div class="row">
                <div class="col-sm-12">
                    <div class="menu-list content-gap">
                        <div class="form-group">                        
                            <label class="form-check-label">
                                Allow dragging menu items                              
                                <label class="switch switch-text switch-pill switch-primary menuHeaderDragSwitch">
                                    <input type="checkbox" name="menuHeaderDragSwitch" class="switch-input menuHeaderDragSwitch">
                                    <span class="switch-label" data-on="On" data-off="Off"></span>
                                    <span class="switch-handle"></span>
                                </label>
                            </label>                     
                        </div>   
                        <div class="menu-drag-container">
                            <ol class="list-group menu-drag menu-header-drag">
                                <?= $menu_header_list; ?>                            
                            </ol>                            
                        </div>
                        <div class="d-flex justify-content-end content-gap contain-full">
                            <button type="button" class="btn btn-primary saveHeaderMenu">Save</button>
                        </div>
                    </div>                
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (!empty($menu_footer_list)): ?>
    <div class="card">
        <div class="card-block">
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="card-title mb-0">Footer Menu</h4>
                    <div class="small text-muted">Arrange footer menu item order.</div>
                </div>                                       
            </div>            
            <div class="row">
                <div class="col-sm-12">
                    <div class="menu-list content-gap">
                        <div class="form-group">                        
                            <label class="form-check-label">
                                Allow dragging menu items                              
                                <label class="switch switch-text switch-pill switch-primary menuFooterDragSwitch">
                                    <input type="checkbox" name="menuFooterDragSwitch" class="switch-input menuFooterDragSwitch">
                                    <span class="switch-label" data-on="On" data-off="Off"></span>
                                    <span class="switch-handle"></span>
                                </label>
                            </label>                     
                        </div>   
                        <div class="menu-drag-container">
                            <ol class="list-group menu-drag menu-footer-drag">
                                <?= $menu_footer_list; ?>       
                            </ol>
                        </div>
                        <div class="d-flex justify-content-end content-gap contain-full">
                            <button type="button" class="btn btn-primary saveFooterMenu">Save</button>
                        </div>
                    </div>                
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>