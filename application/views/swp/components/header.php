<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">
        <meta name="author" content="Łukasz Holeczek">
        <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,AngularJS,Angular,Angular2,Angular 2,Angular4,Angular 4,jQuery,CSS,HTML,RWD,Dashboard,React,React.js,Vue,Vue.js">
        <link rel="shortcut icon" href="img/favicon.png">
        <title>Simplewebpanel</title>
        <!-- Icons -->
        <link href="<?= base_url('assets/swp/css/font-awesome.min.css') ?>" rel="stylesheet">
        <link href="<?= base_url('assets/swp/css/simple-line-icons.css') ?>" rel="stylesheet">        
        <!-- Main styles for this application -->
        <link href="<?= base_url('assets/swp/css/style.css" rel="stylesheet') ?>">
        <!-- outdated browser plugin css -->
        <link href="<?= base_url('assets/swp/css/outdatedbrowser.min.css') ?>" rel="stylesheet">
        <!-- Selectize plugin css -->
        <link href="<?= base_url('assets/swp/css/selectize.css') ?>" rel="stylesheet">
        <!-- nanogallery2 plugin css -->
        <link href="<?= base_url('assets/swp/css/nanogallery2.min.css') ?>" rel="stylesheet">
        <!-- SWP styles -->        
        <link href="<?= base_url('assets/swp/css/swp.css" rel="stylesheet') ?>">
    </head>
    <script>
        var swp = {
            base_url: "<?php echo base_url(); ?>",
            token_hash: "<?php echo $this->security->get_csrf_hash(); ?>"
        };
    </script>
    <?php if ($logged_in): ?>      
        <body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">
            <header class="app-header navbar">
                <button class="navbar-toggler mobile-sidebar-toggler d-lg-none" type="button">☰</button>
                <a class="navbar-brand" href="./"><img src="<?= base_url($swp['dashboard_logo']?:'assets/swp/img/dashboard_logo.png') ?>"></a>
                <ul class="nav navbar-nav d-md-down-none">
                    <li class="nav-item">
                        <a class="nav-link navbar-toggler sidebar-toggler" href="#">☰</a>
                    </li>
                    <li class="nav-item px-3">
                        <a class="nav-link" href="#">Dashboard</a>
                    </li>
                    <li class="nav-item px-3">
                        <a class="nav-link" href="#">Users</a>
                    </li>
                    <li class="nav-item px-3">
                        <a class="nav-link" href="#">Settings</a>
                    </li>
                </ul>
                <ul class="nav navbar-nav ml-auto">
                    <li class="nav-item d-md-down-none">
                        <a class="nav-link" href="#"><i class="icon-bell"></i><span class="badge badge-pill badge-danger">5</span></a>
                    </li>
                    <li class="nav-item d-md-down-none">
                        <a class="nav-link" href="#"><i class="icon-list"></i></a>
                    </li>
                    <li class="nav-item d-md-down-none">
                        <a class="nav-link" href="#"><i class="icon-location-pin"></i></a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="icon-user"></i>  
                            <span class="d-md-down-none">User</span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <div class="dropdown-header text-center">
                                <strong>Account</strong>
                            </div>
                            <a class="dropdown-item" href="#"><i class="fa fa-bell-o"></i> Updates<span class="badge badge-info">42</span></a>
                            <a class="dropdown-item" href="#"><i class="fa fa-envelope-o"></i> Messages<span class="badge badge-success">42</span></a>
                            <a class="dropdown-item" href="#"><i class="fa fa-tasks"></i> Tasks<span class="badge badge-danger">42</span></a>
                            <a class="dropdown-item" href="#"><i class="fa fa-comments"></i> Comments<span class="badge badge-warning">42</span></a>
                            <div class="dropdown-header text-center">
                                <strong>Settings</strong>
                            </div>
                            <a class="dropdown-item" href="#"><i class="fa fa-user"></i> Profile</a>
                            <a class="dropdown-item" href="#"><i class="fa fa-wrench"></i> Settings</a>
                            <a class="dropdown-item" href="#"><i class="fa fa-usd"></i> Payments<span class="badge badge-default">42</span></a>
                            <a class="dropdown-item" href="#"><i class="fa fa-file"></i> Projects<span class="badge badge-primary">42</span></a>
                            <div class="divider"></div>
                            <a class="dropdown-item" href="#"><i class="fa fa-shield"></i> Lock Account</a>
                            <a class="dropdown-item" href="<?= base_url() ?>dashboard/logout"><i class="fa fa-lock"></i> Logout</a>
                        </div>
                    </li>
                    <li class="nav-item d-md-down-none">
                        <a class="nav-link navbar-toggler aside-menu-toggler" href="#">☰</a>
                    </li>
                </ul>
            </header>
            <div class="app-body">
            <?php else: ?>
                <body class="app swp">
                    <header class="app-header navbar">            
                        <a class="navbar-brand" href="#"><img src="<?= base_url($swp['dashboard_logo']?:'assets/swp/img/dashboard_logo.png') ?>"></a>
                        <ul class="nav navbar-nav d-md-down-none justify-content-end">
                            <?php if ($segment['method'] == 'register'): ?>
                                <li class="nav-item px-3">
                                    <a class="nav-link" href="<?= base_url() ?>admin/login">Login</a>
                                </li>
                                <li class="nav-item px-3"></li>
                                <?php endif; ?>                                       
                        </ul>
                    </header>                  
                <?php endif; ?>

