<?php $this->load->view('swp/components/header'); ?>
<?php $this->load->view('swp/components/sidebar'); ?>
<!-- Main content -->
<main class="main">
    <!-- Breadcrumb -->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Admin</li>
        <li class="breadcrumb-item <?= !$segment['method'] ? 'active' : '' ?>"><a href="./">Dashboard</a></li>
        <li class="breadcrumb-item <?= $segment['method'] ? 'active' : '' ?>"><?= ucwords($segment['method']) ?></li>
        <!-- Breadcrumb Menu-->
        <li class="breadcrumb-menu d-md-down-none">
            <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                <a class="btn btn-secondary" href="#"><i class="icon-speech"></i></a>
                <a class="btn btn-secondary" href="./"><i class="icon-graph"></i> &nbsp;Dashboard</a>
                <a class="btn btn-secondary" href="#"><i class="icon-settings"></i> &nbsp;Settings</a>
            </div>
        </li>
    </ol>
    <div class="container-fluid">
        <div class="animated fadeIn">           
            <?php if(!empty($site_content))$this->load->view($site_content); ?>
        </div>
    </div>
        <!-- /.conainer-fluid -->
</main>
<?php $this->load->view('swp/components/footer'); ?>

