<?php $this->load->view('swp/components/header'); ?>
<div class="flex-row align-items-center register">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card mx-4">
                    <div class="card-block p-4">
                        <?= form_open('admin/register') ?>
                        <h1>Register</h1>
                        <p class="text-muted">Create an admin login account</p>  
                        <div class="form-group">
                            <div class="input-group <?= form_error('rUsername') ? 'has-danger' : '' ?>">
                                <span class="input-group-addon"><i class="icon-user"></i>
                                </span>
                                <input type="text" name="rUsername" value="<?= set_value('rUsername'); ?>" class="form-control" placeholder="Username">                               
                            </div>   
                            <?= form_error('rUsername', '<div class="alert alert-danger" role="alert">', '</div>') ?>
                        </div>
                        <div class="form-group">
                            <div class="input-group <?= form_error('rEmail') ? 'has-danger' : '' ?>">
                                <span class="input-group-addon">@</span>
                                <input type="text" name="rEmail" value="<?= set_value('rEmail'); ?>" class="form-control" placeholder="Email">
                            </div> 
                            <?= form_error('rEmail', '<div class="alert alert-danger" role="alert">', '</div>') ?>
                        </div>  
                        <div class="form-group">
                            <div class="input-group <?= form_error('rPassword') ? 'has-danger' : '' ?>">
                                <span class="input-group-addon"><i class="icon-lock"></i>
                                </span>
                                <input type="password" name="rPassword" class="form-control" placeholder="Password">
                            </div>   
                            <?= form_error('rPassword', '<div class="alert alert-danger" role="alert">', '</div>') ?>
                        </div>   
                        <div class="form-group">
                            <div class="input-group <?= form_error('rConfirmPassword') ? 'has-danger' : '' ?>">
                                <span class="input-group-addon"><i class="icon-lock"></i>
                                </span>
                                <input type="password" name="rConfirmPassword" class="form-control" placeholder="Repeat password">
                            </div> 
                            <?= form_error('rConfirmPassword', '<div class="alert alert-danger" role="alert">', '</div>') ?>
                        </div>                        
                        <button type="submit" class="btn btn-block btn-success">Create Account</button>
                        <?= $alert; ?>
                        <?= form_close(); ?>
                    </div>                   
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('swp/components/footer'); ?>