<?php $this->load->view('swp/components/header'); ?>
<div class="flex-row align-items-center register">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card mx-4">
                    <div class="card-block p-4">
                        <?= form_open('admin/login') ?>
                        <h1>Login</h1>
                        <p class="text-muted">Sign In to your account</p>      
                        <div class="form-group">
                            <div class="input-group <?= form_error('lUsernameEmail') ? 'has-danger' : '' ?>">
                                <span class="input-group-addon"><i class="icon-user"></i></span>
                                <input type="text" name="lUsernameEmail" value="<?= set_value('lUsernameEmail'); ?>" class="form-control" placeholder="Email / Username">                        
                            </div>                      
                            <?= form_error('lUsernameEmail', '<div class="alert alert-danger" role="alert">', '</div>') ?>
                        </div>
                        <div class="form-group">
                            <div class="input-group <?= form_error('lPassword') ? 'has-danger' : '' ?>">
                                <span class="input-group-addon"><i class="icon-lock"></i>
                                </span>
                                <input type="password" name="lPassword" class="form-control" placeholder="Password">                           
                            </div>   
                            <?= form_error('lPassword', '<div class="alert alert-danger" role="alert">', '</div>') ?>
                        </div>                        
                        <div class="row justify-content-center">
                            <div class="col-3">
                                <button type="submit" class="btn btn-primary px-4 ">Login</button>
                            </div>                            
                        </div>
                        <?= $alert; ?>
                        <?= form_close(); ?>
                    </div>                   
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('swp/components/footer'); ?>