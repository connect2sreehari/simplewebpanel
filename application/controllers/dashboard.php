<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Dashboard extends SWP_Admin {
    public function __construct() {
        parent::__construct();
    }
    // showing default page view
    public function index() {
        $this->load->view('swp/index', $this->data);
    }
    public function old() {
        $this->load->view('swp/old', $this->data);
    }
    // loading website info 
    public function website() {
        $this->load->helper('upload');
        $this->data['segment']['method'] = "Website";
        $this->load->model('site_model');
        $this->data['site'] = array(
            'name' => '',
            'meta_tag' => '',
            'logo' => '',
            'dashboard_logo' => '',
            'favicon' => ''
        );
        $row = $this->site_model->getSiteInfo();
        if (count($row)) {
            $siteinfo = & $this->data['site'];
            $siteinfo['name'] = $row->site_name;
            $siteinfo['meta_tag'] = $row->site_meta_tag;
            $siteinfo['logo'] = $row->site_logo;
            $siteinfo['dashboard_logo'] = $row->site_logo_dashboard;
            $siteinfo['favicon'] = $row->site_favicon;
        }
        $this->load->library('form_validation');
        $validation_rules = array(
            array('field' => 'siteTitle', 'label' => 'Site name', 'rules' => 'trim|required|regex_match[/^[A-Za-z0-9 ]*$/]',
                'errors' => array(
                    'required' => 'Site name required.',
                    'regex_match' => 'Please use only letters (A-Z, a-z), numbers and space.'
                )),
            array('field' => 'siteMetaTag', 'label' => 'Site metadata', 'rules' => 'trim'),
            array('field' => 'siteLogoUpload', 'label' => 'Site Logo', 'rules' => array(
                    array('validate_logo', function() {
                            if ($_FILES['siteLogoUpload']['error'] != 4) {
                                $logo_config = array(
                                    'extension' => array('jpg', 'gif', 'png', 'jpeg'),
                                    'size' => 10240
                                );
                                $check_logo_file = check_upload_file($_FILES['siteLogoUpload'], $logo_config);
                                if ($check_logo_file) {
                                    $this->form_validation->set_message('validate_logo', $check_logo_file);
                                    return FALSE;
                                }
                                return TRUE;
                            } else {
                                return TRUE;
                            }
                        })
                )),
            array('field' => 'siteDashboardLogoUpload', 'label' => 'Dashboard logo', 'rules' => array(
                    array('validate_dashboard_logo', function() {
                            if ($_FILES['siteDashboardLogoUpload']['error'] != 4) {
                                $dashboard_logo_config = array(
                                    'extension' => array('jpg', 'gif', 'png', 'jpeg'),
                                    'width' => 130,
                                    'height' => 50,
                                    'size' => 10240
                                );
                                $check_dashboard_logo_file = check_upload_file($_FILES['siteDashboardLogoUpload'], $dashboard_logo_config);
                                if ($check_dashboard_logo_file) {
                                    $this->form_validation->set_message('validate_dashboard_logo', $check_dashboard_logo_file);
                                    return FALSE;
                                }
                                return TRUE;
                            } else {
                                return TRUE;
                            }
                        })
                )),
            array('field' => 'siteFaviconUpload', 'label' => 'Favicon', 'rules' => array(
                    array('validate_favicon', function() {
                            if ($_FILES['siteFaviconUpload']['error'] != 4) {
                                $favicon_config = array(
                                    'extension' => array('ico'),
                                    'size' => 10240
                                );
                                $check_favicon_file = check_upload_file($_FILES['siteFaviconUpload'], $favicon_config);
                                if ($check_favicon_file) {
                                    $this->form_validation->set_message('validate_favicon', $check_favicon_file);
                                    return FALSE;
                                }
                                return TRUE;
                            } else {
                                return TRUE;
                            }
                        })
                ))
        );
        $this->form_validation->set_rules($validation_rules);
        if ($this->form_validation->run() == TRUE) {
            $this->load->library('upload');
            if ($_FILES['siteLogoUpload']['error'] != 4) {
                $config['upload_path'] = 'assets/swp/uploads';
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['file_name'] = 'logo';
                $config['overwrite'] = TRUE;
                $path_logo = $this->data['site']['logo'];
                $this->upload->initialize($config, TRUE);
                if (!$this->upload->do_upload('siteLogoUpload')) {
                    $this->form_validation->set_message('validate_logo', $this->upload->display_errors('', ''));
                } else {
                    $upload_data = $this->upload->data();
                    if (!empty($upload_data)) {
                        if (!empty($this->data['site']['logo'])) {
                            unlink(FCPATH . $path_logo);
                        }
                        $path_logo = $config['upload_path'] . '/' . $upload_data['file_name'];
                    }
                }
            }
            if ($_FILES['siteDashboardLogoUpload']['error'] != 4) {
                $config['upload_path'] = 'assets/swp/uploads';
                $config['allowed_types'] = 'gif|jpg|jpeg|png';
                $config['file_name'] = 'dashboard_logo';
                $config['max_width'] = '130';
                $config['max_height'] = '50';
                $config['overwrite'] = TRUE;
                $path_dashboard_logo = $this->data['site']['dashboard_logo'];
                $this->upload->initialize($config, TRUE);
                if (!$this->upload->do_upload('siteDashboardLogoUpload')) {
                    $this->form_validation->set_message('validate_dashboard_logo', $this->upload->display_errors('', ''));
                } else {
                    $upload_data = $this->upload->data();
                    if (!empty($upload_data)) {
                        if (!empty($this->data['site']['dashboard_logo'])) {
                            unlink(FCPATH . $path_logo);
                        }
                        $path_dashboard_logo = $config['upload_path'] . '/' . $upload_data['file_name'];
                    }
                }
            }
            if ($_FILES['siteFaviconUpload']['error'] != 4) {
                $config['upload_path'] = 'assets/swp/uploads';
                $config['file_name'] = 'favicon';
                $config['allowed_types'] = 'ico';
                $config['overwrite'] = TRUE;
                $path_favicon = $this->data['site']['favicon'];
                $this->upload->initialize($config, TRUE);
                if (!$this->upload->do_upload('siteFaviconUpload')) {
                    $this->form_validation->set_message('validate_favicon', $this->upload->display_errors('', ''));
                } else {
                    $upload_data = $this->upload->data();
                    if (!empty($upload_data)) {
                        if (!empty($this->data['site']['favicon'])) {
                            unlink(FCPATH . $path_favicon);
                        }
                        $path_favicon = $config['upload_path'] . '/' . $upload_data['file_name'];
                    }
                }
            }
            if ($this->site_model->siteInfoInsert($path_logo, $path_dashboard_logo, $path_favicon)) {
                $this->session->set_flashdata('success', 'Website information saved.');
                redirect('dashboard/website');
            } else {
                $this->session->set_flashdata('error', 'Website information saving failed.');
            }
        }
        $this->data['alert'] = $this->alertMessage() ?: '';
        $this->data['site_content'] = 'swp/components/website';
        $this->load->view('swp/index', $this->data);
    }
    // delete website logo
    public function deleteLogo() {
        if (count($_POST) > 0) {
            $this->load->model('site_model');
            $row = $this->site_model->getSiteInfo();
            if ($this->site_model->deleteLogo()) {
                if (count($row)) {
                    unlink(FCPATH . $row->site_logo);
                }
                echo TRUE;
            } else {
                echo FALSE;
            }
        } else {
            redirect('dashboard/website');
        }
    }
    // delete dashboard logo logo
    public function deleteDashboardLogo() {
        if (count($_POST) > 0) {
            $this->load->model('site_model');
            $row = $this->site_model->getSiteInfo();
            if ($this->site_model->deleteDashboardLogo()) {
                if (count($row)) {
                    unlink(FCPATH . $row->site_logo_dashboard);
                }
                echo TRUE;
            } else {
                echo FALSE;
            }
        } else {
            redirect('dashboard/website');
        }
    }
    // delete favicon logo
    public function deleteFavicon() {
        if (count($_POST) > 0) {
            $this->load->model('site_model');
            $row = $this->site_model->getSiteInfo();
            if ($this->site_model->deleteFavicon()) {
                if (count($row)) {
                    unlink(FCPATH . $row->site_favicon);
                }
                echo TRUE;
            } else {
                echo FALSE;
            }
        } else {
            redirect('dashboard/website');
        }
    }
    // loading menu page
    public function menu($menu_edit_id = NULL) {
        $this->data['segment']['method'] = "Menu";
        $this->load->model('menu_model');
        $this->load->helper('menu');
        $records_header_menu = $this->menu_model->getHeaderMenu();
        $menu_header_list = $this->menu_model->getParentMenuItems($records_header_menu, 'H');
        $records_footer_menu = $this->menu_model->getFooterMenu();
        $menu_footer_list = $this->menu_model->getParentMenuItems($records_footer_menu, 'F');
        $menu_header_drag_list = get_menu_drag_list($menu_header_list, 0);
        $menu_footer_drag_list = get_menu_drag_list($menu_footer_list, 0);
        $this->data['menu_list'] = $this->menu_model->getAllMenu();
        $this->data['menu_header_list'] = $menu_header_drag_list;
        $this->data['menu_footer_list'] = $menu_footer_drag_list;
        $this->data['menu_edit'] = array(
            'menu_name' => '',
            'menu_display_name' => '',
            'menu_link' => '',
            'menu_show_in' => '',
            'menu_parent_id' => '',
            'menu_id' => '',
            'menu_edit_id' => ''
        );
        $menu_edit_id = $this->input->post('menuEdit') ?: $menu_edit_id;
        $menu_edit = & $this->data['menu_edit'];
        $menu_edit['menu_edit_id'] = $menu_edit_id;
        $menu_edit_validation_rules = '';
        if (!empty($menu_edit_id)) {
            $row = $this->menu_model->getAllMenu($menu_edit_id);
            if (count($row) == 0) {
                redirect('dashboard/menu');
            }
            $menu_edit['menu_name'] = $row->menu_name;
            $menu_edit['menu_display_name'] = $row->menu_display_name;
            $menu_edit['menu_link'] = $row->menu_link;
            $menu_edit['menu_show_in'] = $row->menu_show_in;
            $menu_edit['menu_parent_id'] = $row->menu_parent_id;
            $menu_edit['menu_id'] = $row->menu_id;
            $check_menu_item_name = array('validate_menu_item_name', function () {
                    $where = array('menu_id' => $this->input->post('menuEdit'), 'menu_name' => $this->input->post('menuItemName'));
                    if ($this->menu_model->checkRowExists($where, 'menu_name')) {
                        return true;
                    } else {
                        $where = array('menu_name' => $this->input->post('menuItemName'));
                        return !$this->menu_model->checkRowExists($where, 'menu_name');
                    }
                });
            $menu_edit_validation_rules = array('field' => 'menuEdit', 'label' => 'Menu edit id', 'rules' => array('trim', 'regex_match[/^[0-9]*$/]',
                    array('validate_menu_id', function () {
                            return $this->menu_model->checkRowExists(array('menu_id' => $this->input->post('menuEdit')), 'menu_id');
                        })),
                'errors' => array(
                    'regex_match' => 'Invalid menu edit item.',
                    'validate_menu_id' => 'Invalid menu edit item.'
            ));
        } else {
            $check_menu_item_name = 'is_unique[swp_menu.menu_name]';
        }
        $this->load->library('form_validation');
        $validation_rules = array(
            array('field' => 'menuItemName', 'label' => 'Menu item name', 'rules' => array('trim', 'required', 'regex_match[/^[A-Za-z0-9_]*$/]', $check_menu_item_name, 'strtolower'),
                'errors' => array(
                    'required' => 'Menu item name required.',
                    'regex_match' => 'Please use only letters (A-Z, a-z), numbers and underscore.',
                    'is_unique' => 'That Menu name is taken. Try another.1',
                    'validate_menu_item_name' => 'That Menu name is taken. Try another.'
                )),
            array('field' => 'menuParentItem', 'label' => 'Menu parent item', 'rules' => array('trim', 'regex_match[/^[0-9]*$/]',
                    array('validate_parent_id', function () {
                            if ($this->input->post('menuParentItem') == '') {
                                return TRUE;
                            } else {
                                return $this->menu_model->checkRowExists(array('menu_id' => $this->input->post('menuParentItem')), 'menu_id');
                            }
                        }),
                    array('validate_HF_Switch', function () {
                            if (!empty($this->input->post('menuParentItem'))) {
                                $switchOn = menuHFSwitch($this->input->post('menuHeaderFooterSwitch[]'));
                                $where = array('menu_id' => $this->input->post('menuParentItem'), 'menu_show_in' => $switchOn);
                                return $this->menu_model->checkRowExists($where, 'menu_id');
                            } else {
                                return TRUE;
                            }
                        })),
                'errors' => array(
                    'regex_match' => 'Invalid parent menu.',
                    'validate_parent_id' => 'Invalid parent menu.',
                    'validate_HF_Switch' => 'Cannot do this check parent menu item belong to header or footer.'
                )),
            array('field' => 'menuDisplayName', 'label' => 'Menu display name', 'rules' => 'trim|required|regex_match[/^[A-Za-z0-9 ]*$/]',
                'errors' => array(
                    'required' => 'Menu display name required.',
                    'regex_match' => 'Please use only letters (A-Z, a-z), numbers and space.'
                )),
            array('field' => 'menuItemLink', 'label' => 'Menu item link', 'rules' => 'trim|required',
                'errors' => array(
                    'required' => 'Menu item link required.'
                )),
            array('field' => 'menuHeaderFooterSwitch[]', 'label' => 'Header or Footer Switch', 'rules' => array('trim', 'required', 'strtoupper'),
                'errors' => array(
                    'required' => 'Header or Footer switch must be on.'
                )),
            $menu_edit_validation_rules
        );
        $this->form_validation->set_rules($validation_rules);
        if ($this->form_validation->run() == TRUE) {
            if ($this->menu_model->insertmenuitem()) {
                if (!empty($menu_edit_id)) {
                    $msg = 'Menu item updated successfully.';
                } else {
                    $msg = 'Menu item created successfully.';
                }
                $this->session->set_flashdata('success', $msg);
                redirect('dashboard/menu');
            } else {
                if (!empty($menu_edit_id)) {
                    $msg = 'Menu item updation unsuccessful.';
                } else {
                    $msg = 'Menu item creation unsuccessful.';
                }
                $this->session->set_flashdata('error', $msg);
            }
        }
        $this->data['alert'] = $this->alertMessage() ?: '';
        $this->data['site_content'] = 'swp/components/menu';
        $this->load->view('swp/index', $this->data);
    }
    // saving menu header item 
    public function saveHeaderMenu() {
        if (count($_POST) > 0) {
            $this->load->helper('menu');
            $menu_data = $this->input->post('menuData');
            $menu_order = getSavedOrder($menu_data[0]);
            $this->load->model('menu_model');
            $this->menu_model->updateMenuOrder($menu_order, 'H');
            echo TRUE;
        } else {
            redirect('dashboard/menu');
        }
    }
    // saving menu footer item 
    public function saveFooterMenu() {
        if (count($_POST) > 0) {
            $this->load->helper('menu');
            $menu_data = $this->input->post('menuData');
            $menu_order = getSavedOrder($menu_data[0]);
            $this->load->model('menu_model');
            $this->menu_model->updateMenuOrder($menu_order, 'F');
            echo TRUE;
        } else {
            redirect('dashboard/menu');
        }
    }
    // create article
    public function article() {
        $this->data['segment']['method'] = "Article";
        $this->load->model('menu_model');
        $this->data['page_list'] = $this->menu_model->getAllMenu();
        $this->load->library('form_validation');
        $validation_rules = array(
            array('field' => 'page', 'label' => 'Page', 'rules' => array('trim', 'required', 'regex_match[/^[0-9]*$/]', array('validate_page_exists', function() {
                            if (!empty($this->input->post('page'))) {
                                $where = array('menu_id' => $this->input->post('page'));
                                return $this->menu_model->checkRowExists($where, 'menu_id');
                            }
                            return TRUE;
                        })),
                'errors' => array(
                    'required' => 'Please choose a page for article.',
                    'regex_match' => 'Invalid page.',
                    'validate_page_exists' => 'Invalid page.'
                ))
        );
        $this->form_validation->set_rules($validation_rules);
        if ($this->form_validation->run() == TRUE) {
            
        }
        $this->data['site_content'] = 'swp/components/article';
        $this->load->view('swp/index', $this->data);
    }
    // loading album
    public function album() {
        $this->load->model('album_model');
        $this->load->model('gallery_model');
        $albums = $this->album_model->getAlbum();
        $image_gallery = '';
        $j = 0;
        foreach ($albums as $album_row) {
            $i = 0;
            $gallery = $this->gallery_model->getGallery($album_row->album_id);
            foreach ($gallery as $gallery_row) {
                $image_path = base_url($gallery_row->image_path);
                $image_thumb_path = base_url($gallery_row->image_thumb_path);
                $image_title = $gallery_row->image_title ?: '--';
                $image_description = $gallery_row->image_description ?: '--';
                if ($j != 0 && $i == 0) {
                    $image_gallery .= ",";
                }
                if ($i == 0) {
                    $image_gallery .= "{'srct': '$image_thumb_path', 'title': '$album_row->album_name', 'ID': $album_row->album_id, 'kind': 'album', 'customData': {'favorite': false}}";
                }
                $image_gallery .= ",{'src': '$image_path', 'srct': '$image_thumb_path', 'title': '$image_title', 'description':'$image_description', 'ID':$gallery_row->image_id, 'albumID': $gallery_row->swp_album_album_id, 'customData': {'favorite': false}}";
                $i++;
                $j++;
            }
        }
        $this->data['albums'] = $albums;
        $this->data['image_gallery'] = $image_gallery;
        $this->data['site_content'] = 'swp/components/album';
        $this->load->view('swp/index', $this->data);
    }
    // create album 
    public function createAlbum() {
        if (count($_POST) > 0) {
            $this->load->model('album_model');
            // check album on creating 
            if (!empty($this->input->post('albumName'))) {
                $albumName = trim($this->input->post('albumName'));
                if (!preg_match('/^[A-Za-z0-9 ]*$/', $albumName)) {
                    echo 'Only letters, numbers and whitespace allowed.';
                } else if ($this->album_model->albumNameExists($albumName) > 0) {
                    echo 'This album name already exists.';
                } else {
                    if ($this->album_model->createAlbum()) {
                        echo TRUE;
                    } else {
                        echo 'Error occured on creating album.';
                    }
                }
            } else {
                echo 'Album name required.';
            }
        } else {
            redirect('dashboard/album');
        }
    }
    // update album 
    public function updateAlbum() {
        if (count($_POST) > 0) {
            $this->load->model('album_model');
            // check album on creating 
            if (!empty($this->input->post('albumName'))) {
                $albumName = trim($this->input->post('albumName'));
                if (!preg_match('/^[A-Za-z0-9 ]*$/', $albumName)) {
                    echo 'Only letters, numbers and whitespace allowed.';
                } else if ($this->album_model->albumNameExists($albumName) > 0) {
                    echo 'This album name already exists.';
                } else if (empty($this->input->post('albumId'))) {
                    echo 'Album id required.';
                } else if (!preg_match('/^[0-9]*$/', $this->input->post('albumId'))) {
                    echo 'Invalid album id.';
                } else {
                    if ($this->album_model->createAlbum($this->input->post('albumId'))) {
                        echo TRUE;
                    } else {
                        echo 'Error occured on updating album.';
                    }
                }
            } else {
                echo 'Album name required.';
            }
        } else {
            redirect('dashboard/album');
        }
    }
    //delete album
    public function deleteAlbum() {
        if (count($_POST) > 0) {
            $this->load->model('album_model');
            $this->load->model('gallery_model');
            if ($this->input->post('albumId') > 0) {
                $imageGallery = $this->gallery_model->getGallery($this->input->post('albumId'));
                foreach ($imageGallery as $image) {
                    unlink(FCPATH . $image->image_path);
                    unlink(FCPATH . $image->image_thumb_path);
                }
                $this->gallery_model->deleteGallery($this->input->post('albumId'));
                $this->album_model->deleteAlbum($this->input->post('albumId'));
            }
            return TRUE;
        } else {
            redirect('dashboard/album');
        }
    }
    // album image upload
    public function albumImageUpload() {
        if (count($_POST) > 0) {
            $this->load->library('upload');
            $this->load->library('image_lib');
            $this->load->model('album_model');
            $this->load->model('gallery_model');
            $selectedAlbum = '';
            if (!empty($this->input->post('selectAlbum'))) {
                $selectedAlbum = $this->input->post('selectAlbum');
                if (count($this->album_model->getAlbum($selectedAlbum)) == 0) {
                    echo 'Invalid album name.';
                    exit;
                }
            } else {
                echo 'Album name required.';
                exit;
            }
            if ($_FILES['albumUploadFile']['error'] != 4) {
                $config['upload_path'] = 'assets/swp/uploads/album';
                $config['allowed_types'] = 'gif|png|jpeg|jpg';
                $config['encrypt_name'] = TRUE;
                $this->upload->initialize($config, TRUE);
                if (!$this->upload->do_upload('albumUploadFile')) {
                    echo $this->upload->display_errors('' . $_FILES['albumUploadFile']['name'] . ' - ', '');
                } else {
                    $upload_data = $this->upload->data();
                    $thumb_config['source_image'] = $upload_data['full_path'];
                    $thumb_config['new_image'] = 'thumb_' . $upload_data['file_name'];
                    $thumb_config['image_library'] = 'gd2';
                    $thumb_config['create_thumb'] = FALSE;
                    $thumb_config['maintain_ratio'] = TRUE;
                    $thumb_config['width'] = 300;
                    $thumb_config['height'] = 150;
                    $this->image_lib->initialize($thumb_config);
                    $this->image_lib->resize();
                    $this->image_lib->clear();
                    $uploaded_path = $config['upload_path'] . '/' . $upload_data['file_name'];
                    $uploaded_thumb_path = $config['upload_path'] . '/thumb_' . $upload_data['file_name'];
                    $image = array('album_id' => $selectedAlbum, 'path' => $uploaded_path, 'thumb_path' => $uploaded_thumb_path);
                    if (!$this->gallery_model->createGalleryImage($image)) {
                        echo $_FILES['albumUploadFile']['name'] . ' - ' . "Error occured.";
                    }
                }
            } else {
                echo 'No file selected';
            }
        } else {
            redirect('dashboard/album');
        }
    }
    //delete gallery image
    public function deleteGalleryImage() {
        if (count($_POST) > 0) {
            $this->load->model('gallery_model');
            if ($this->input->post('imageId') > 0) {
                $imageGallery = $this->gallery_model->getGalleryImage($this->input->post('imageId'));
                unlink(FCPATH . $imageGallery->image_path);
                unlink(FCPATH . $imageGallery->image_thumb_path);
                $this->gallery_model->deleteGalleryImage($this->input->post('imageId'));
            }
            return TRUE;
        } else {
            redirect('dashboard/album');
        }
    }
    // logout from dashboard
    public function logout() {
        $this->load->model('user_model');
        $this->user_model->logout();
        redirect('admin/login');
    }
}