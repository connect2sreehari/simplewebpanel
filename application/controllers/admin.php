<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Admin extends SWP_Admin {
    function __construct() {
        parent::__construct();
    }
    // loading login and setting migration
    public function index() {
        $this->login();
        $this->migration(6);
    }
    // loading register for first admin, it only work when there is no admin account
    public function register() {
        $this->load->model('user_model');
        if ($this->user_model->userExists()) {
            redirect('admin/login');
        }
        $this->load->library('form_validation');
        $validationRules = array(
            array('field' => 'rUsername', 'label' => 'Username', 'rules' => 'trim|required|regex_match[/^[A-Za-z0-9]*$/]|min_length[5]|max_length[15]|is_unique[swp_user.username]|strtolower',
                'errors' => array(
                    'required' => 'Username required.',
                    'username_check' => 'Username name already exists. Please use a different username.',
                    'regex_match' => 'Please use only letters (A-Z, a-z) and numbers.',
                    'is_unique' => 'That Username is taken. Try another.'
                )
            ),
            array('field' => 'rEmail', 'label' => 'Email', 'rules' => 'trim|required|valid_email|max_length[255]|is_unique[swp_user.user_email]|strtolower',
                'errors' => array(
                    'required' => 'Email required.',
                    'valid_email' => 'Invalid email address.',
                    'is_unique' => 'That Email address is taken. Try another.'
                )
            ),
            array('field' => 'rPassword', 'label' => 'Password', 'rules' => 'required',
                'errors' => array(
                    'required' => 'Password required.'
                )
            ),
            array('field' => 'rConfirmPassword', 'label' => 'Confirm Password', 'rules' => 'required|matches[rPassword]',
                'errors' => array(
                    'required' => 'Confirm password required.',
                    'matches' => 'Password and confirm password don\'t match.'
                )
            )
        );
        $this->form_validation->set_rules($validationRules);
        if ($this->form_validation->run() == TRUE) {
            if ($this->user_model->register()) {
                $this->session->set_flashdata('success', 'Account created successfully.');
                redirect('admin/register');
            } else {
                $this->session->set_flashdata('error', 'Account creation unsuccessful.');
            }
        }
        $this->data['alert'] = $this->alertMessage() ?: '';
        $this->load->view('swp/register', $this->data);
    }
    // loading login 
    public function login() {
        $this->load->library('form_validation');
        $validationRules = array(
            array('field' => 'lUsernameEmail', 'label' => 'Username', 'rules' => 'trim|required|strtolower',
                'errors' => array(
                    'required' => 'Username required.'
                )
            ),
            array('field' => 'lPassword', 'label' => 'Password', 'rules' => 'required',
                'errors' => array(
                    'required' => 'Password required.'
                )
            )
        );
        $this->form_validation->set_rules($validationRules);
        if ($this->form_validation->run() == TRUE) {
            $this->load->model('user_model');
            if (count($this->user_model->login()) > 0) {
                $result = $this->user_model->login();
                $session_data = array(
                    'user_id' => $result->user_id,
                    'username' => $result->username,
                    'user_type' => $result->user_type,
                    'logged_in' => TRUE
                );
                $this->session->set_userdata($session_data);
                redirect('dashboard/website');
            } else if (!$this->user_model->userExists()) {
                redirect('admin/register');
            } else {
                $this->session->set_flashdata('error', 'Invalid Username and Password.');
                $this->data['alert'] = $this->alertMessage() ?: '';
            }
        }
        $this->load->view('swp/login', $this->data);
    }
}