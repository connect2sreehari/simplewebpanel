# Simplewebpanel #

Simplewebpanel is an panel that allows to control basic features of site like menu options, banners images, slider images,
article posting and editing, image gallery, contact us form , contact us address, social network links. Simplewebpanel UI created using coreui admin panel.
The codeigniter framework is used to add give server side functionality. 

## Project under development ##

### Current completed project areas ###

* Login to panel
* Set default settings like site name, site logo, site dashboard logo, favicon
* Menu option creating, editing, deleting

### Used in project ###

### Template ###

* Free CoreUI admin panel

### Used javascript plugins ###

* outdated browser
* selectize
* sortable
* bPopup

### Framework ###

* CodeIgniter(PHP framework)

### License ###

The MIT License (MIT)

Copyright (c) 2014 - 2017, Sreehari

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.